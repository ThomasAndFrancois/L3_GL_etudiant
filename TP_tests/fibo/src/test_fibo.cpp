#include "Fibo.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, test1) {

    CHECK_EQUAL(fibo(0), 0);
    CHECK_EQUAL(fibo(1), 1);
    CHECK_EQUAL(fibo(2), 1);
    CHECK_EQUAL(fibo(3), 2);
    CHECK_EQUAL(fibo(4), 3);

}
