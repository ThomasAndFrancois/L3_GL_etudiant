#! /usr/bin/env python3

import repeat
import fibo

def print_fibo(n):
	fii = fibo.fiboIterative(n)
	print(fii)
	
repeat.repeatN(10,print_fibo)
