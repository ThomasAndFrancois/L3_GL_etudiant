#! /usr/bin/env python3

def repeatN(n,f):
	for i in range(1, n+1):
		f(i)
