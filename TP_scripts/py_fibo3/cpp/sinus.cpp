#include <cmath>

float fSinus(float x,float a,float b){
	float result;
	result = sin(2*M_PI*((a*x)+b));
	return result;
}


#include <pybind11/pybind11.h>
PYBIND11_PLUGIN(sinus) {
    pybind11::module m("sinus");
    m.def("fSinus", &fSinus);
    return m.ptr();
}
