#!/bin/sh

#g++ -Wall -Wextra -o sayHello.out sayHello.cpp module1.cpp

g++ -Wall -Wextra -c sayHello.cpp
g++ -Wall -Wextra -c module1.cpp
g++ -Wall -Wextra -o sayHello.out sayHello.o module1.o
